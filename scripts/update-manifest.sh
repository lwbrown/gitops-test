#!/bin/bash
set -e

jq \
  --arg FILENAME "manifest.yaml" \
  --arg MESSAGE "Updated manifest for $CI_COMMIT_BRANCH" \
  --rawfile TEMPLATE ./manifest.yaml \
  '.actions[0].content = $TEMPLATE | .commit_message = $MESSAGE | .actions[0].file_path = $FILENAME' \
  ./scripts/commit.tpl.json > ./commit_payload.json

curl \
  --data "@commit_payload.json" \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN"  \
  "https://gitlab.gitlab.inspectorga.xyz/api/v4/projects/$MANIFEST_PROJECT_ID/repository/commits" > status_code

echo status_code

# if [[ $(cat status_code) -lt 400 ]] ; then
#   exit 0
# else
#   echo "error"
#   exit 1
# fi
